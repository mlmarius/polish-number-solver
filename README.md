# Problem
Infix expression:   (1 + 2) * 3

Prefix expression:  * + 1 2 3

Value: 9

---

Infix expression:   6 + ((4 - (2 + 3)) * 8)

Prefix expression:  + 6 * - 4 + 2 3 8

Value: -2

---

Infix expression:   6 + ((4 - (2 + 3)) * 8)

Prefix expression:  + 6 * - 4 + 2 3 8

---

Should also be able to evaluate expression across an input domain




