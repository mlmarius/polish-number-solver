import string
from collections import deque
from itertools import product
from typing import Dict, Optional, Tuple, Iterable
from contextlib import suppress


VariableDomainSpec = Tuple[int, int]
VariablesContainer = Dict[str, VariableDomainSpec]
VariableDomain = Dict[str, Iterable[int]]
VariablesInput = Dict[str, int]

OPERATORS = ('+', '-', '*', '/')


def _is_operator(token: string) -> bool:
    return token in OPERATORS


def _is_variable(token: string) -> bool:
    return token in string.ascii_letters


def _translate_polish(tokens: deque) -> str:
    token = tokens.popleft()

    if _is_operator(token):
        if token == '/':
            token = '//'
        return '(' + _translate_polish(tokens) + token + _translate_polish(tokens) + ')'

    if _is_variable(token):
        return f'{{{token}}}'

    return token


def _compute_variable_domains(variables: VariablesContainer) -> VariableDomain:
    return {key: range(low, high) for key, (low, high) in variables.items()}


def _compute_variable_combinations(domains: VariableDomain) -> Iterable[VariablesInput]:
    keys = domains.keys()
    ranges = [domains[key] for key in keys]
    variable_combos = product(*ranges)
    for combo in variable_combos:
        expression_params = zip(keys, combo)
        yield dict(expression_params)


def _do_math(expression: str) -> int:
    return eval(expression)


def _replace_variables(expression: str, values: VariablesInput) -> str:
    return expression.format(**values)


def _evaluate_for_variables(expression: str, variables: Dict[str, int]) -> int:
    expr = _replace_variables(expression, variables)
    return _do_math(expr)


def _evaluate_across_domain(expression: str, variables: VariablesContainer) -> int:
    variable_domains = _compute_variable_domains(variables)
    combos = _compute_variable_combinations(variable_domains)
    results = [_evaluate_for_variables(expression, variables) for variables in combos]
    return max(results)


def _into_stack(expression: str) -> deque:
    return deque(expression.split())


def max_result_expression(expression: str, variables: Optional[VariablesContainer] = None) -> Optional[int]:
    with suppress(Exception):
        expr = _translate_polish(_into_stack(expression))
        if variables and len(variables.keys()):
            return _evaluate_across_domain(expr, variables)
        return _do_math(expr)
