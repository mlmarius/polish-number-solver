from main import max_result_expression, _translate_polish, _into_stack


def test_parses_simple():
    assert _translate_polish(_into_stack('+ 1 2')) == '(1+2)'


def test_parse_with_variables():
    assert _translate_polish(_into_stack('+ x * - y + 2 3 8')) == '({x}+(({y}-(2+3))*8))'


def test_division():
    assert max_result_expression('/ 10 5') == 2
    assert max_result_expression('/ 10 3') == 3


def test_computes_simple():
    assert max_result_expression('* + 1 2 3') == 9
    assert max_result_expression('+ 6 * - 4 + 2 3 8') == -2


def test_computes_with_variables():
    assert max_result_expression(
        '+ x * - 4 + 2 y z',
        {
            'x': (1, 3),
            'y': (2, 10),
            'z': (5, 20)
        }
    ) == 2


def test_none_if_errors():
    assert max_result_expression('gibberish') is None
